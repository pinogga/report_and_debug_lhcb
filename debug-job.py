import os
from zipfile import ZipFile
import subprocess
import argparse
import sys
import tempfile

parser = argparse.ArgumentParser(description = "At the moment rurunning to easily reproduce DaVinci errors")
parser.add_argument("URL",help="Enter the LogSE URL to rerun the job",type=str,nargs="?")
parser.add_argument("-id","--step_id",help="If multiple log files are found in URL, please specify step ID",nargs="?",type=int)
args = parser.parse_args()


#TODO ADD DOCUMENTATION 
#MAKE SURE YOU HAVE A KERBEROS TICKET!
#Run: [ERROR] Server responded with an error: [3010] Unable to give access - user access restricted - unauthorized identity used ; Permission denied (source)
#Take a look at Permission Error!
URL = args.URL
step_id = args.step_id
if URL[-1] == "/":
    postfix = URL.split("ch/")[-1][:-1]
    zip_postfix = URL.split("/")[-2]
else:
    postfix = URL.split("ch/")[-1]
    zip_postfix = URL.split("/")[-1]
print("\n")

print("Downloading ZIP file from EOS into temporary file... \n")
print("EOS location: /eos/lhcb/grid/prod/lhcb/logSE/{0}.zip \n".format(postfix))
with tempfile.NamedTemporaryFile() as tmp:
    try:
        p = subprocess.check_call(["xrdcp","-f","root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/logSE/{0}.zip".format(postfix),tmp.name])
    except subprocess.CalledProcessError:
        print("Oops. Something went wrong. Make sure the URL is correct and you have a valid Kerberos ticket!")
        sys.exit()
    with ZipFile(tmp.name) as zf:
        with tempfile.TemporaryDirectory() as tmp2: 
            zf.extractall(tmp2)
            onlyfiles = zf.namelist()
            msk_log = [".log" in files for files in onlyfiles]
            msk_json = [".json" in files for files in onlyfiles]
            onlylogs = [onlyfiles[i] for i in range(len(onlyfiles)) if msk_log[i]]
            onlylogs.sort()
            onlyjson = [onlyfiles[i] for i in range(len(onlyfiles)) if msk_json[i]]
            onlyjson.sort()
            print("Available steps are:")
            for i, log in enumerate(onlylogs):
                print("{0}. : {1}".format(i+1,onlylogs[i].strip(".log")))
            print("\n")

            if len(onlylogs)>1 and step_id is None:
                print("Found {0} steps, please pass -id or --step_id to continue!".format(len(onlylogs)))
                sys.exit()
            if step_id is None:
                step_id = 1 
            else:
                pass

            if len(onlylogs)<step_id:
                print("Found {0} steps, you specified step {1}. Choose a valid step number using --step_id".format(len(onlylogs),step_id))
                sys.exit()
            else:
                pass
            msk_rerun = [onlylogs[step_id-1].strip(".log").split("/")[-1] in files for files in onlyjson]
            run_json = [onlyjson[i] for i in range(len(onlyjson)) if msk_rerun[i]]
            print("Debugging step {0} ({1})".format(step_id,onlylogs[step_id-1].strip(".log")))
            print("Running step {0} interactively \n".format(step_id))
            for file in run_json:
                process = subprocess.Popen(["lb-prod-run","--interactive",tmp2+"/{1}".format(zip_postfix,file)])
                process.communicate()

            #TODO Do we wish to keep the files after the job runs? Yes!5z%4^O5hFifQ
            #TODO the jobs sometimes also produce some output files? (lb-exec yaml files etc..) Remove after test or keep? Keep!
    



